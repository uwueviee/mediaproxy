# mediaproxy: mediaproxy component of litecord
# Copyright 2018-2019, Luna Mendes and the mediaproxy contributors
# SPDX-License-Identifier: AGPL-3.0-only

import urllib.parse
from typing import Optional, Dict, Any

from quart import current_app as app
from logbook import Logger

from mediaproxy.cache import url_tuple
from mediaproxy.bp.proxy import get_meta

log = Logger(__name__)


async def embed_image(image_url_unparsed: str) -> Optional[Dict[str, Any]]:
    """Generate a Discord Embed image object out of a url."""
    image_url = urllib.parse.urlparse(image_url_unparsed)
    query_append = f"?{image_url.query}" if image_url.query else ""
    img_raw_url = f"{image_url.netloc}{image_url.path}{query_append}"

    img_url, img_id = url_tuple(image_url.scheme, img_raw_url)

    log.debug("querying image {} {}", img_url, img_id)
    meta = await get_meta(img_url, img_id)

    # construct a proxy url based off app.cfg['mediaproxy']['domain']
    md_domain = app.cfg["mediaproxy"]["domain"]
    sec = "s" if app.cfg["mediaproxy"]["tls"] else ""

    img_proxy_url = f"http{sec}://{md_domain}/img/" f"{image_url.scheme}/{img_raw_url}"

    if meta["image"]:
        return {
            "height": meta["height"],
            "width": meta["width"],
            "proxy_url": img_proxy_url,
            "url": img_url,
        }

    return None

async def inject_opengraph_image(embed, meta_content: str):
    """Inject embed.thumbnail based on an og:image meta tag"""
    image = await embed_image(meta_content)

    if image:
        embed["thumbnail"] = image

async def embed_video(video_url_unparsed: str) -> Optional[Dict[str, Any]]:
    video_url = urllib.parse.urlparse(video_url_unparsed)
    query_append = f"?{video_url.query}" if video_url.query else ""
    video_raw_url = f"{video_url.netloc}{video_url.path}{query_append}"

    vid_url, vid_id = url_tuple(video_url.scheme, video_raw_url)

    log.debug("querying video {} {}", vid_url, vid_id)
    meta = await get_meta(vid_url, vid_id)

    # construct a proxy url based off app.cfg['mediaproxy']['domain']
    md_domain = app.cfg["mediaproxy"]["domain"]
    sec = "s" if app.cfg["mediaproxy"]["tls"] else ""

    video_proxy_url = f"http{sec}://{md_domain}/img/" f"{video_url.scheme}/{video_raw_url}"

    if meta["video"]:
        return {
            "height": meta["height"],
            "width": meta["width"],
            "proxy_url": video_proxy_url,
            "url": vid_url,
        }

    return None

async def inject_opengraph_video(embed, meta_content: str):
    """Inject embed.video based on an og:video meta tag"""
    video = await embed_video(meta_content)

    if video:
        embed["video"] = video

def parse_url_to_tuple(url: str) -> tuple:
    url_lib = urllib.parse.urlparse(url)
    query_append = f"?{url_lib.query}" if url_lib.query else ""
    raw_url = f"{url_lib.netloc}{url_lib.path}{query_append}"

    return url_tuple(url_lib.scheme, raw_url)