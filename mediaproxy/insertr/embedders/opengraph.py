# mediaproxy: mediaproxy component of litecord
# Copyright 2018-2019, Luna Mendes and the mediaproxy contributors
# SPDX-License-Identifier: AGPL-3.0-only

from enum import Enum

from bs4 import BeautifulSoup
from logbook import Logger

from mediaproxy.errors import InvalidResponse
from mediaproxy.utils import inject_opengraph_image, inject_opengraph_video
from mediaproxy.outgoing import fetch

log = Logger(__name__)


class KnownSites(Enum):
    NOT_KNOWN = 0
    YOUTUBE = 1
    TENOR = 2
    GIPHY = 3


async def from_html(html: str, known: KnownSites) -> dict:
    """Generate an embed out of raw HTML information."""
    # NOTE: the lxml html parser is much better than python's.
    # i was having trouble understanding its meta tags on python.
    soup = BeautifulSoup(html, "lxml")

    embed = {}
    head = soup.head

    # Apply types and providers for known sites
    match known:
        case KnownSites.YOUTUBE:
            embed = {"type": "video", "provider": {"name": "YouTube", "url": "https://www.youtube.com"}, "author": {},
                     "video": {}, "color": 16711680}
        case KnownSites.TENOR:
            embed = {"type": "gifv", "provider": {"name": "Tenor", "url": "https://tenor.co"}}
        case KnownSites.GIPHY:
            embed = {"type": "gifv", "provider": {"name": "Giphy", "url": "https://giphy.com/"}}

    if not head:
        return {}

    for child in head.children:
        # skip non-<meta>
        if child.name != "meta":
            continue

        try:
            meta_prop = child["property"]
            meta_content = child["content"]
        except KeyError:
            continue

        # TODO: We should look at the og:type prop and maybe try to resolve it
        if known == KnownSites.NOT_KNOWN:
            embed["type"] = "link"

        if meta_prop == "og:title":
            embed["title"] = meta_content

        if meta_prop == "og:description":
            embed["description"] = meta_content

        if meta_prop == "og:image":
            await inject_opengraph_image(embed, meta_content)

        # we can't proxy the youtube embed, so we just use the raw data provided
        if known == KnownSites.YOUTUBE:
            if meta_prop == "og:video:url":
                embed["video"]["url"] = meta_content

            if meta_prop == "og:video:width":
                embed["video"]["width"] = meta_content

            if meta_prop == "og:video:height":
                embed["video"]["height"] = meta_content
        else:
            if meta_prop == "og:video":
                await inject_opengraph_video(embed, meta_content)

    return embed


async def gen_embed(url: str) -> dict:
    """Generate an embed based on OpenGraph data. Requests the HTML off
    the page and feeds to the from_html function."""
    try:
        text = await fetch(url)
    except InvalidResponse:
        return {}

    embed = await from_html(text, KnownSites.NOT_KNOWN)

    if embed:
        # inject embed.url
        embed["url"] = url

    return embed
