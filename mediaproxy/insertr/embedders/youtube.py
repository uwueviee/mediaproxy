# mediaproxy: mediaproxy component of litecord
# Copyright 2018-2019, Luna Mendes and the mediaproxy contributors
# SPDX-License-Identifier: AGPL-3.0-only

from logbook import Logger

from mediaproxy.outgoing import fetch

from .opengraph import from_html, KnownSites

log = Logger(__name__)


async def gen_embed(url: str) -> dict:
    """Make an embed out of simple meta tags."""
    text = await fetch(url)
    embed = await from_html(text, KnownSites.YOUTUBE)

    if embed:
        # inject embed.url
        embed["url"] = url

        # inject oembed data
        oembed = await fetch("https://www.youtube.com/oembed?format=json&url=" + url, json=True)

        embed["author"]["name"] = oembed["author_name"]
        embed["author"]["url"] = oembed["author_url"]

    return embed
