# mediaproxy: mediaproxy component of litecord
# Copyright 2018-2019, Luna Mendes and the mediaproxy contributors
# SPDX-License-Identifier: AGPL-3.0-only

from logbook import Logger

from mediaproxy.bp.proxy import get_meta

from mediaproxy.utils import parse_url_to_tuple, embed_image, embed_video

log = Logger(__name__)


async def gen_embed(url: str) -> dict:
    log.debug("media fallback embedding!")

    url_tuple = parse_url_to_tuple(url)
    meta = await get_meta(url_tuple[0], url_tuple[1])

    embed = {}

    if meta["image"]:
        embed = {"type": "image", "thumbnail": await embed_image(url)}

    elif meta["video"]:
        embed = {"type": "video", "video": await embed_video(url)}

    if embed:
        # inject embed.url
        embed["url"] = url

    return embed
